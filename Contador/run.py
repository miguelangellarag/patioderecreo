from vunit import VUnit

# Allow the use of call()
from subprocess import call

# Create VUnit instance by parsing command-line arguments
vu = VUnit.from_argv()
vu.add_vhdl_builtins()

# Enable the communication library ('com')
vu.add_com()

# Enable OSVVM (Open Source VHDL Verification Methology)2
vu.add_osvvm()

# Create library 'src_lib'
lib = vu.add_library("src_lib")

# Add all files ending in .vhd in current working directory to library
lib.add_source_files("*.vhd")

# Enable code coverage collection
#lib.set_sim_option("enable_coverage", True)
#lib.set_compile_option("enable_coverage", True)

# Use the post_run hook to tell VUnit that it has to merge coverage data from
# all tests (with gcovr) and create the html reports (with lcov and genhtml)
#def post_run(results):
#    results.merge_coverage(file_name="coverage_data")
#    if vu.get_simulator_name() == "ghdl":
#        call(["gcovr", "coverage_data"])
#        call(["lcov", '--capture', '--directory', '.', '--output', 'coverage.info'])
#        call(["genhtml", "coverage.info", "--output-directory", "coverage_report"])

# Run vunit function, but pass to it2 the post_run hook
#vu.main(post_run=post_run)

vu.main()